FROM golang:1.14-alpine AS builder

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o payroll ./cmd/payroll/main.go

FROM alpine

RUN apk add ca-certificates

COPY --from=builder /app/payroll /app/payroll
COPY --from=builder /app/config /app/config

WORKDIR /app

EXPOSE 8080

CMD ["/app/payroll"]