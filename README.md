# Wave Software Development Challenge

Applicants for the Full-stack Developer role at Wave must
complete the following challenge, and submit a solution prior to the onsite
interview.

The purpose of this exercise is to create something that we can work on
together during the onsite. We do this so that you get a chance to collaborate
with Wavers during the interview in a situation where you know something better
than us (it's your code, after all!)

There isn't a hard deadline for this exercise; take as long as you need to
complete it. However, in terms of total time spent actively working on the
challenge, we ask that you not spend more than a few hours, as we value your
time and are happy to leave things open to discussion in the on-site interview.

Please use whatever programming language and framework you feel the most
comfortable with.

Feel free to email [dev.careers@waveapps.com](dev.careers@waveapps.com) if you
have any questions.

## Project Description

Imagine that this is the early days of Wave's history, and that we are prototyping a new payroll system API. A front end (that hasn't been developed yet, but will likely be a single page application) is going to use our API to achieve two goals:

1. Upload a CSV file containing data on the number of hours worked per day per employee
1. Retrieve a report detailing how much each employee should be paid in each _pay period_

All employees are paid by the hour (there are no salaried employees.) Employees belong to one of two _job groups_ which determine their wages; job group A is paid $20/hr, and job group B is paid $30/hr. Each employee is identified by a string called an "employee id" that is globally unique in our system.

Hours are tracked per employee, per day in comma-separated value files (CSV).
Each individual CSV file is known as a "time report", and will contain:

1. A header, denoting the columns in the sheet (`date`, `hours worked`,
   `employee id`, `job group`)
1. 0 or more data rows

In addition, the file name should be of the format `time-report-x.csv`,
where `x` is the ID of the time report represented as an integer. For example, `time-report-42.csv` would represent a report with an ID of `42`.

You can assume that:

1. Columns will always be in that order.
1. There will always be data in each column and the number of hours worked will always be greater than 0.
1. There will always be a well-formed header line.
1. There will always be a well-formed file name.

A sample input file named `time-report-42.csv` is included in this repo.

### What your API must do:

We've agreed to build an API with the following endpoints to serve HTTP requests:

1. An endpoint for uploading a file.

   - This file will conform to the CSV specifications outlined in the previous section.
   - Upon upload, the timekeeping information within the file must be stored to a database for archival purposes.
   - If an attempt is made to upload a file with the same report ID as a previously uploaded file, this upload should fail with an error message indicating that this is not allowed.

1. An endpoint for retrieving a payroll report structured in the following way:

   _NOTE:_ It is not the responsibility of the API to return HTML, as we will delegate the visual layout and redering to the front end. The expectation is that this API will only return JSON data.

   - Return a JSON object `payrollReport`.
   - `payrollReport` will have a single field, `employeeReports`, containing a list of objects with fields `employeeId`, `payPeriod`, and `amountPaid`.
   - The `payPeriod` field is an object containing a date interval that is roughly biweekly. Each month has two pay periods; the _first half_ is from the 1st to the 15th inclusive, and the _second half_ is from the 16th to the end of the month, inclusive. `payPeriod` will have two fields to represent this interval: `startDate` and `endDate`.
   - Each employee should have a single object in `employeeReports` for each pay period that they have recorded hours worked. The `amountPaid` field should contain the sum of the hours worked in that pay period multiplied by the hourly rate for their job group.
   - If an employee was not paid in a specific pay period, there should not be an object in `employeeReports` for that employee + pay period combination.
   - The report should be sorted in some sensical order (e.g. sorted by employee id and then pay period start.)
   - The report should be based on all _of the data_ across _all of the uploaded time reports_, for all time.

   As an example, given the upload of a sample file with the following data:

    <table>
    <tr>
      <th>
        date
      </th>
      <th>
        hours worked
      </th>
      <th>
        employee id
      </th>
      <th>
        job group
      </th>
    </tr>
    <tr>
      <td>
        2020-01-04
      </td>
      <td>
        10
      </td>
      <td>
        1
      </td>
      <td>
        A
      </td>
    </tr>
    <tr>
      <td>
        2020-01-14
      </td>
      <td>
        5
      </td>
      <td>
        1
      </td>
      <td>
        A
      </td>
    </tr>
    <tr>
      <td>
        2020-01-20
      </td>
      <td>
        3
      </td>
      <td>
        2
      </td>
      <td>
        B
      </td>
    </tr>
    <tr>
      <td>
        2020-01-20
      </td>
      <td>
        4
      </td>
      <td>
        1
      </td>
      <td>
        A
      </td>
    </tr>
    </table>

   A request to the report endpoint should return the following JSON response:

   ```javascript
   {
     payrollReport: {
       employeeReports: [
         {
           employeeId: 1,
           payPeriod: {
             startDate: "2020-01-01",
             endDate: "2020-01-15"
           },
           amountPaid: "$300.00"
         },
         {
           employeeId: 1,
           payPeriod: {
             startDate: "2020-01-16",
             endDate: "2020-01-31"
           },
           amountPaid: "$80.00"
         },
         {
           employeeId: 2,
           payPeriod: {
             startDate: "2020-01-16",
             endDate: "2020-01-31"
           },
           amountPaid: "$90.00"
         }
       ];
     }
   }
   ```

We consider ourselves to be language agnostic here at Wave, so feel free to use any combination of technologies you see fit to both meet the requirements and showcase your skills. We only ask that your submission:

- Is easy to set up
- Can run on either a Linux or Mac OS X developer machine
- Does not require any non open-source software

### Documentation:

Please commit the following to this `README.md`:

1. Instructions on how to build/run your application
1. Answers to the following questions:
   - How did you test that your implementation was correct?
   - If this application was destined for a production environment, what would you add or change?
   - What compromises did you have to make as a result of the time constraints of this challenge?

## Submission Instructions

1. Clone the repository.
1. Complete your project as described above within your local repository.
1. Ensure everything you want to commit is committed.
1. Create a git bundle: `git bundle create your_name.bundle --all`
1. Email the bundle file to [dev.careers@waveapps.com](dev.careers@waveapps.com) and CC the recruiter you have been in contact with.

## Evaluation

Evaluation of your submission will be based on the following criteria.

1. Did you follow the instructions for submission?
1. Did you complete the steps outlined in the _Documentation_ section?
1. Were models/entities and other components easily identifiable to the
   reviewer?
1. What design decisions did you make when designing your models/entities? Are
   they explained?
1. Did you separate any concerns in your application? Why or why not?
1. Does your solution use appropriate data types for the problem as described?


## Building And Running The Application

For the following instructions there are 2 requirements for building and running this application:
- docker
- docker-compose (you can technically run it without this but its easier to get the database up running along side the application with this)

To run the application navigate to the projects root directory in your terminal and run the following command:
`docker-compose up -d --build`

This should build and run the application along side postgres docker image and a pgAdmin docker image. Once the command has finished you can make
requests against the following endpoints of the application:
1. **To submit a time report** 

    Send a POST request to the endpoint `localhost:8000/time-report/upload` with your time report csv file attached to the
    body as form-data under the key `time-report`. Below is an example curl request to do this
    ```
    curl --location --request POST 'http://localhost:8000/time-report/upload' --form 'time-report=@/C:/Users/Tyler Mizuyabu/Documents/time-report-42.csv'
    ```
    Just replace the "/C:/Users/Tyler Mizuyabu/Documents/time-report-42.csv" with the path to your csv file.

2. **To generate a payroll report**
   
   Send a GET request to the endpoint `localhost:8000/payroll/report`
   Below is an example curl request to do this
   ```
   curl --location --request GET 'http://localhost:8000/payroll/report'
   ```
  The output isn't formatted very nicely so I would probably pipe the result into a json file to view it. Or if you have the `json_pp` command you can pipe
  it into that.

## How did I test that my implementation was correct?

  I created test csv files for different test cases, such as a file with just the headers and no content, I file with no content, a file with no .csv, a file with dates that havent been formated as dd/mm/yyyy to test against.

  I also made sure to write tests for the functions within the util.go file (although I wish I has written more but I wanted to get the project in sooner than later) since that file contained the majority of the business logic in the application and if there was a bug it was more likely to be found there.

## If this application was destined for a production environment, what would you add or change?
  - Way more logs, and some form of request tracing. Monitoring an application in a prod environment is very difficult without this.
  - I would add validation for the incomming time report csv files (such as checking to see if an employee logged more than 24 hours of work in a single day)
  - I would definitely add more tests to the project, especially for the functions within the utils.go file (which lacked unit tests for some of the functions and failure test cases for all of them). This file contains the majority of the business logic for the application so it's very important for the functions within this file to be properly tested before moving to prod.

   As a side note something that I would probably work towards changing, but would only do before production if time wasnt a factor, would be to refactor the utils.go file. At the moment a lot of logic is is encapsulated within the util.go file that could probably be spread out to multiple files. I also think there are some functions there that would be more appropriately defined on related types (such as `createEmployeeReport`).

## What compromises did you have to make as a result of the time constraints of this challenge?

  There were a couple of assumptions that were made when starting this challenge:

  1. For the sake of simplicity, I made the following assumptions about the input file and therefore didn't code any checks or extra logic for these cases:
        - It won't contain multiple entries within the file for the same date and same employee (the application doesn't actually break when this occurs
          but it won't aggregrate the the entries into a single entry for that day and it doesn't check that the total hours is <= 24)
          - ex: 
            ```
            date,hours worked,employee id,job group
            14/11/2016,7.5,1,A
            14/11/2016,4,1,A
            ```
        - It won't contain multiple entries within the file for the same employee and same date, but with different groups (once again this doesn't break the
          application but a usecase where an employee switched job types within a day seemed odd and maybe something to check for if I were performing validation
          on the input file)
          - ex:
            ```
            date,hours worked,employee id,job group
            14/11/2016,7.5,1,A
            14/11/2016,4,1,B
            ```
          - the following version however is stil ok:
            ```
            date,hours worked,employee id,job group
            14/11/2016,7.5,1,A
            15/11/2016,4,1,B
            ```
        - There won't be an entry in the timesheet that contains a number of hours greater than 24 or a total sum of hours within a day to be greater than 24
    
  2. For generating the payroll report I wanted to look into other ways to either query the data or maintain data within the database in such a way that it would be 
    easier to generate teh report and wouldn't require so much code to turn a list of time entries into a list of employee reports. There were a couple of other approaches that I was looking into:
      - **Method1**: Performing multiple queries to the database using payment period start and end date as conditionals. In this method I would determine which entries in the database have the earliest & latest timestamps for the date they were entered and then generate a list of payment periods from that contain all time periods from the start date to the end date. Then send queries to the db for each range using the start and end date as conditionals and grouping by employee id and job group of the entry while aggregating all hours for the employee in order to get sum hours of the employee for that payment period

        However I had the following issues with this method:
        - Its more network intesive which could be a problem depending on the applications connection with the db
        - I don't think it does much in terms of simplifying the code base
     - **Method2**: Modify the entry table to include a colume that stores the start date of the payment period a time entry belongs to. This column would be could be maintained by a Postgres trigger that occurs on creating/updating a row in the table. Then when I query the table I can group by employeeId, paymentPeriod start date, job group and perform aggregation on the hours. The data would be in a closer format to the end result for the employee reports and should make the conversion into employee reports easier

        However I had the following issues with this method:
        - My lack of SQL knowledge. I lack too much knowledge about SQL and Postgres to do this implementation within a timely matter. I'm not even sure how possible this is and whether or not the idea is just a crazy pipe dream or super convoluted. It would have probably lead to a pretty buggy implementation
        - This method is Postgres specific and wouldn't be flexible if the application was to switch databases later on
        - The database now needs to know information about the business domain and I have lost separation of logic which can always gets messy.
        - If the application decides to support different payment period formats later on (like monthy instead of just bi weekly) it will be a pain to change.
    
    So in the end I chose to do a simpler implementation that I would be more confident in delivering on time and with less errors.

  3. I didn't test as thoroughly, and my project layout could have been more thoughtful. I could have also documented my code alot better.