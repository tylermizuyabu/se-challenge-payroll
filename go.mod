module se-challenge-payroll

go 1.14

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	github.com/ilyakaznacheev/cleanenv v1.2.4
	github.com/jinzhu/gorm v1.9.14
	github.com/jinzhu/now v1.0.1
	github.com/labstack/echo/v4 v4.1.16
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.4.0
)
