- Can there be multiple entries within the file for the same date and same employee?
  - ex: 
    ```
    date,hours worked,employee id,job group
    14/11/2016,7.5,1,A
    14/11/2016,4,1,A
    ```
- Can there be multiple entries within the file for the same employee with different groups? 
  - ex:
    ```
    date,hours worked,employee id,job group
    14/11/2016,7.5,1,A
    14/11/2016,4,1,B
    9/11/2016,4,1,B
    ```
  - if yes, then is the file considered invalid?
    - I'm thinking that it should be considered invalid if the entries occur on the same day
- Is it possible for an entry in the timesheet to contain a number of hours greater than 24?
  - if yes, then is the file considered invalid?


---

Based on email feedback:
- The "degenerate" cases described above are invalid and won't have to be dealt with in the code, which means:
  - The input file won't contain an entry with the same date and same employee
  - The won't be multiple entries within the file for the same  employee with different groups
  - It's not possible for an entry to contain more than 24 hours