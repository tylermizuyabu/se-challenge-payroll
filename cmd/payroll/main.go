package main

import (
	"fmt"
	"se-challenge-payroll/internal/interfaces"
	"se-challenge-payroll/internal/payroll"
	"se-challenge-payroll/internal/repositories/pgrepo"
	"se-challenge-payroll/internal/types"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
)

var cfg types.Config
var db *gorm.DB

func main() {
	err := cleanenv.ReadConfig("config/config.yaml", &cfg)
	if err != nil {
		panic("Error occurred attempting to load server configuration")
	}

	if cfg.Production {
		logrus.SetLevel(logrus.WarnLevel)
	} else {
		logrus.SetLevel(logrus.DebugLevel)
	}
	logrus.WithField("config", cfg).Debug("Loaded configurations")

	db, err = gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s", cfg.Host, cfg.PGConfig.Port, cfg.User, cfg.Database, cfg.Password, cfg.SSLMode))
	if err != nil {
		logrus.WithError(err).Fatal("Error occurred attempting to open db connection")
	}
	defer db.Close()

	err = db.AutoMigrate(&types.TimeReportEntryDal{}, &types.TimeReportDal{}).Error
	if err != nil {
		logrus.WithError(err).Warnf("Error occurred created table for models")
	}

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	timeReportRepo := new(interfaces.ITimeReportRepository)
	*timeReportRepo = pgrepo.NewTimeReportRepository(db)
	timeReportEntryRepo := new(interfaces.ITimeReportEntryRepository)
	*timeReportEntryRepo = pgrepo.NewTimeReportEntryRepository(db)

	payroll.NewPayrollRest(timeReportRepo, timeReportEntryRepo, cfg).RegisterHandlers(e)

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", cfg.ServerPort)))
}
