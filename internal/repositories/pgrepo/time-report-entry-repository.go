package pgrepo

import (
	"fmt"
	"se-challenge-payroll/internal/types"

	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

type TimeReportEntryRepository struct {
	db *gorm.DB
}

func NewTimeReportEntryRepository(db *gorm.DB) *TimeReportEntryRepository {
	return &TimeReportEntryRepository{
		db: db,
	}
}

func (ter *TimeReportEntryRepository) Get() ([]types.TimeReportEntryDal, error) {
	entries := make([]types.TimeReportEntryDal, 0)
	tx := ter.db.Order("employee_id asc, date asc").Find(&entries)
	err := tx.Error
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"Transaction": fmt.Sprintf("%+v", tx)}).Warn("Error occurred attempting to retrieve TimeReportEntries from the database")
		return nil, err
	}
	logrus.WithFields(logrus.Fields{"Transaction": fmt.Sprintf("%+v", tx), "Value": fmt.Sprintf("%+v", entries)}).Print("Retrieved TimeReportEntries")
	return entries, nil
}
