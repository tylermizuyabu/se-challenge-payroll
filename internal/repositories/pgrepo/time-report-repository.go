package pgrepo

import (
	"errors"
	"fmt"
	"se-challenge-payroll/internal/types"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

var (
	DuplicateKey = errors.New("Duplicate primary key")
)

type TimeReportRepository struct {
	db *gorm.DB
}

func NewTimeReportRepository(db *gorm.DB) *TimeReportRepository {
	return &TimeReportRepository{
		db: db,
	}
}

func (tr *TimeReportRepository) Create(t *types.TimeReportDal) error {
	tx := tr.db.Create(&t)
	err := tx.Error
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"Transaction": fmt.Sprintf("%+v", tx), "TimeReport": t}).Warn("Error occurred attempting to create a TimeReport")
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return DuplicateKey
		}
		return err
	}
	logrus.WithFields(logrus.Fields{"Transaction": fmt.Sprintf("%+v", tx), "Value": fmt.Sprintf("%+v", tx.Value)}).Debug("Created TimeReport")
	return nil
}
