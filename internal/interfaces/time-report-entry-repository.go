package interfaces

import "se-challenge-payroll/internal/types"

type ITimeReportEntryRepository interface {
	Get() ([]types.TimeReportEntryDal, error)
}
