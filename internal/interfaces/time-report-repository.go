package interfaces

import "se-challenge-payroll/internal/types"

type ITimeReportRepository interface {
	Create(t *types.TimeReportDal) error
}
