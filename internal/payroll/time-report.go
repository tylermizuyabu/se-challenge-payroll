package payroll

import (
	"fmt"
	"se-challenge-payroll/internal/interfaces"
	"se-challenge-payroll/internal/repositories/pgrepo"

	"github.com/labstack/echo/v4"
)

type TimeReportHandler struct {
	tr            interfaces.ITimeReportRepository
	uploadFileKey string
}

func NewTimeReportHandler(repo *interfaces.ITimeReportRepository, uploadFileKey string) *TimeReportHandler {
	return &TimeReportHandler{
		tr:            *repo,
		uploadFileKey: uploadFileKey,
	}
}

func (th *TimeReportHandler) Create(c echo.Context) error {
	fileHeader, err := c.FormFile(th.uploadFileKey)
	if err != nil {
		return c.String(400, fmt.Sprintf("No csv file found, check the form-data key to ensure that a file under the key %s has been uploaded", th.uploadFileKey))
	}

	fileContent, err := readCSVFormFile(fileHeader)

	if err != nil {
		return c.String(400, "Unable to parse the csv file")
	}

	if len(fileContent) <= 1 {
		return c.String(400, "Time report csv file doesn't contain any entries")
	}

	timeReport, err := createTimeReport(fileContent, fileHeader.Filename)
	if err != nil {
		return c.String(400, err.Error())
	}
	err = th.tr.Create(timeReport)
	if err == pgrepo.DuplicateKey {
		return c.String(400, fmt.Sprintf("Time report with id %d already exists", timeReport.ID))
	}
	return err
}
