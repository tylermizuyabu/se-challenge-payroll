package payroll

import (
	"encoding/csv"
	"mime/multipart"
	"regexp"
	"se-challenge-payroll/internal/types"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/now"
)

/**
	createTimeReport function creates a new TimeReportDal from a 2d slice of strings that represents the
	csv file content and the fileName string of the uploaded csv file
	On success it will return the TimeReportDal struct and nil for the error
	On failure it will return nil and the error that occurred
**/
func createTimeReport(fileContent [][]string, fileName string) (*types.TimeReportDal, error) {
	reportID, err := parseTimeReportID(fileName)
	if err != nil {
		return nil, err
	}

	timeReport := &types.TimeReportDal{
		ID:            reportID,
		ReportEntries: make([]types.TimeReportEntryDal, len(fileContent)-1),
	}

	for i, line := range fileContent[1:] {
		entry, err := types.TimeReportEntryDalFromCSVLine(line)
		if err != nil {
			return nil, err
		}
		timeReport.ReportEntries[i] = *entry
	}

	return timeReport, nil
}

/**
	readCSVFormFile uses the provided fileHeader to read the csv file from the multipart form.
	On success it returns a 2d sice of strings representing the content of the csv file and nil for the error
	On failure it will return nil and the error that occurred
**/
func readCSVFormFile(fileHeader *multipart.FileHeader) ([][]string, error) {
	file, err := fileHeader.Open()
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return csv.NewReader(file).ReadAll()
}

/**
	parseTimeReportID uses a provided fileName to extract the time report's ID as an integer
	This function assumes the fileName provided will be of the format "time-report-x.csv", where x is the report ID and
	the file extension of .csv can either not exist or be duplicated any number of times
	On success it returns the report ID as an int and nil for the error
	On failure it returns 0 for the ID and the error that occurred
**/
func parseTimeReportID(fileName string) (int, error) {
	noExtensionRegex := regexp.MustCompile("(\\.csv)*\\z")
	idString := strings.Split(noExtensionRegex.ReplaceAllString(fileName, ""), "-")[2]
	return strconv.Atoi(idString)
}

/**
	timeReportEntriesToBiWeeklyEmployeeReports converts a list of time report entries into a list of bi-weekly employee reports.
	This method assumes that the entries slice comming is sorted by employee id and date
	On succes it returns a list of employee reports an nil for the error
	On failure it returns nil and the error that occurred
**/
func timeReportEntriesToBiWeeklyEmployeeReports(entries []types.TimeReportEntryDal) ([]types.EmployeeReportDto, error) {
	employeeReports := make([]types.EmployeeReportDto, 0)
	currentReportIndex := -1
	for _, e := range entries {
		payPeriod := getBiWeeklyPayPeriod(e.Date)
		if currentReportIndex < 0 || !employeeReports[currentReportIndex].PayPeriod.Equal(payPeriod) {
			r, err := createEmployeeReport(e, payPeriod)
			if err != nil {
				return nil, err
			}
			currentReportIndex++
			employeeReports = append(employeeReports, *r)
		} else {
			r := employeeReports[currentReportIndex]
			err := r.AmountPaid.AddToAmount(e.Hours, e.Group)
			if err != nil {
				return nil, err
			}
			employeeReports[currentReportIndex] = r
		}
	}

	return employeeReports, nil
}

/**
createEmployeeReport creates a new employee report using info from the provided time report entry
and the payPeriodDto.
On success it returns an EmployeeReportDto and nil for the error
On failure it returns nil and the error that occurred
**/
func createEmployeeReport(e types.TimeReportEntryDal, payPeriod *types.PayPeriodDto) (*types.EmployeeReportDto, error) {
	np := types.NewNetPay(types.AmericanCurrency)
	err := np.AddToAmount(e.Hours, e.Group)
	if err != nil {
		return nil, err
	}
	return &types.EmployeeReportDto{
		EmployeeId: e.EmployeeID,
		PayPeriod:  payPeriod,
		AmountPaid: np,
	}, nil
}

/**
getBiWeeklyPayPeriod returns a payPeriod whose start and end date create a range that contains the provided Time. And
is a valid bi-weekly pay period (either starts on the 1st or 16th and ends on the 15th or End Of Month respectively)
**/
func getBiWeeklyPayPeriod(d time.Time) *types.PayPeriodDto {
	startDate := now.New(d).BeginningOfMonth()
	endDate := startDate.AddDate(0, 0, 14)
	if d.Day() > 15 {
		startDate = endDate.AddDate(0, 0, 1)
		endDate = now.New(startDate).EndOfMonth()
	}

	return &types.PayPeriodDto{
		StartDate: types.FormattedTime(startDate),
		EndDate:   types.FormattedTime(endDate),
	}
}
