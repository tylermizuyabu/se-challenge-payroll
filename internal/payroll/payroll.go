package payroll

import (
	"se-challenge-payroll/internal/interfaces"

	"github.com/labstack/echo/v4"
)

type PayrollHandler struct {
	ter interfaces.ITimeReportEntryRepository
}

func NewPayRollHandler(repo *interfaces.ITimeReportEntryRepository) *PayrollHandler {
	return &PayrollHandler{
		ter: *repo,
	}
}

func (ph *PayrollHandler) GetReport(c echo.Context) error {
	timeReportEntries, err := ph.ter.Get()
	if err != nil {
		return err
	}
	employeeReports, err := timeReportEntriesToBiWeeklyEmployeeReports(timeReportEntries)
	if err != nil {
		return err
	}
	return c.JSON(200, employeeReports)
}
