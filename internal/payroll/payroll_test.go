package payroll

import (
	"fmt"
	"math"
	"se-challenge-payroll/internal/types"
	"strings"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit"
	"github.com/jinzhu/now"
	"github.com/stretchr/testify/assert"
)

func TestParseTimeReportIdSuccess(t *testing.T) {
	reportID := gofakeit.Number(1, 50)
	fileName := fmt.Sprintf("time-report-%d.csv", reportID)

	actualID, err := parseTimeReportID(fileName)
	assert.Equal(t, nil, err)
	assert.Equal(t, reportID, actualID)
}

func TestCreateTimeReportSuccess(t *testing.T) {
	expectedReport := types.TimeReportDal{
		ID:            gofakeit.Number(1, 100),
		ReportEntries: make([]types.TimeReportEntryDal, 0),
	}
	mockReportContent := [][]string{
		strings.Split("date,hours worked,employee id,job group", ","),
	}
	for i := 0; i < 10; i++ {
		e := types.TimeReportEntryDal{
			Date:       time.Date(gofakeit.Year(), time.Month(gofakeit.Number(0, 11)), gofakeit.Day(), 0, 0, 0, 0, time.UTC),
			Hours:      math.Floor(gofakeit.Float64Range(1, 24) * 100 / 100),
			EmployeeID: gofakeit.Number(1, 100),
			Group:      getRandomGroup(),
		}
		expectedReport.ReportEntries = append(expectedReport.ReportEntries, e)
		mockReportContent = append(mockReportContent, []string{e.Date.Format(types.InputDateLayout), fmt.Sprintf("%f", e.Hours), fmt.Sprintf("%d", e.EmployeeID), fmt.Sprintf("%s", e.Group)})
	}

	actualReport, err := createTimeReport(mockReportContent, fmt.Sprintf("time-report-%d.csv", expectedReport.ID))
	assert.Equal(t, nil, err)
	assert.Equal(t, expectedReport, *actualReport)
}

func getRandomGroup() types.JobGroup {
	return []types.JobGroup{types.JobGroup(types.A), types.JobGroup(types.B)}[gofakeit.Number(0, 1)]
}

func TestGetBiWeeklyPayPeriodSuccess(t *testing.T) {
	for i := 0; i < 5; i++ {
		date := time.Date(gofakeit.Year(), time.Month(gofakeit.Number(0, 11)), gofakeit.Number(1, 15), 0, 0, 0, 0, time.UTC)
		expectedPayPeriod := types.PayPeriodDto{
			StartDate: types.FormattedTime(now.New(date).BeginningOfMonth()),
			EndDate:   types.FormattedTime(time.Date(date.Year(), date.Month(), 15, 0, 0, 0, 0, time.UTC)),
		}
		actualPayPeriod := getBiWeeklyPayPeriod(date)
		assert.Equal(t, expectedPayPeriod, *actualPayPeriod)
	}

	for i := 0; i < 5; i++ {
		date := time.Date(gofakeit.Year(), time.Month(gofakeit.Number(0, 11)), gofakeit.Number(16, 30), 0, 0, 0, 0, time.UTC)
		expectedPayPeriod := types.PayPeriodDto{
			StartDate: types.FormattedTime(time.Date(date.Year(), date.Month(), 16, 0, 0, 0, 0, time.UTC)),
			EndDate:   types.FormattedTime(now.New(date).EndOfMonth()),
		}
		actualPayPeriod := getBiWeeklyPayPeriod(date)
		assert.Equal(t, expectedPayPeriod, *actualPayPeriod)
	}

}
