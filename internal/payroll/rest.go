package payroll

import (
	"se-challenge-payroll/internal/interfaces"
	"se-challenge-payroll/internal/types"

	"github.com/labstack/echo/v4"
)

type PayrollRest struct {
	th TimeReportHandler
	ph PayrollHandler
}

func NewPayrollRest(thRepo *interfaces.ITimeReportRepository, terRepo *interfaces.ITimeReportEntryRepository, cfg types.Config) *PayrollRest {
	return &PayrollRest{
		th: *NewTimeReportHandler(thRepo, cfg.TimeReportFileKey),
		ph: *NewPayRollHandler(terRepo),
	}
}

func (rest *PayrollRest) RegisterHandlers(e *echo.Echo) {
	e.POST("/time-report/upload", rest.th.Create)
	e.GET("/payroll/report", rest.ph.GetReport)
}
