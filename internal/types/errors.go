package types

import "errors"

var (
	InvalidDateForm   = errors.New("File contained invalid date formats, dates must be formated as dd/mm/yyyy")
	InvalidHours      = errors.New("File contained invalid hours format, hours must be numbers")
	InvalidEmployeeId = errors.New("File contained invalid employeeID, employee ids must be integers")
	InvalidGroup      = errors.New("File contained invalid job group type")
)
