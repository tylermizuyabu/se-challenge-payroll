package types

const (
	A JobGroup = "A"
	B JobGroup = "B"
)

// JobGroup type is created to store a related set of constants for job groups
type JobGroup string

func (jg *JobGroup) Valid() bool {
	return *jg == A || *jg == B
}

func (jg *JobGroup) GetRate() (float64, error) {
	switch *jg {
	case A:
		return 20.0, nil
	case B:
		return 30.0, nil
	default:
		return 0, InvalidGroup
	}
}
