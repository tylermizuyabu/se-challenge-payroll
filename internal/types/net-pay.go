package types

import "fmt"

const (
	AmericanCurrency string = "$"
)

type NetPay struct {
	amount float64
	symbol string
}

func NewNetPay(symbol string) *NetPay {
	return &NetPay{
		amount: 0,
		symbol: symbol,
	}
}

func (np *NetPay) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%q", np.String())), nil
}

func (np *NetPay) String() string {
	return fmt.Sprintf("%s%0.2f", np.symbol, np.amount)
}

func (np *NetPay) AddToAmount(hours float64, jobGroup JobGroup) error {
	rate, err := jobGroup.GetRate()
	if err != nil {
		return err
	}
	np.amount += hours * rate
	return nil
}
