package types

type EmployeeReportDto struct {
	EmployeeId int           `json:"employeeId"`
	PayPeriod  *PayPeriodDto `json:"payPeriod"`
	AmountPaid *NetPay       `json:"amountPaid"`
}
