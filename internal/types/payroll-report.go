package types

type PayrollReportDto struct {
	EmployeeReports []EmployeeReportDto `json:"employeeReports"`
}
