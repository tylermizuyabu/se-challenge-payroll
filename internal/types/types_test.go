package types

import (
	"fmt"
	"math"
	"testing"

	"github.com/brianvoe/gofakeit"
	"github.com/stretchr/testify/assert"
)

func TestParseDateSuccess(t *testing.T) {
	date := gofakeit.Date()
	dateString := date.Format(InputDateLayout)

	parsedDate, err := parseDateString(dateString)

	assert.Equal(t, nil, err)
	assert.Equal(t, date.Year(), parsedDate.Year())
	assert.Equal(t, date.Month(), parsedDate.Month())
	assert.Equal(t, date.Day(), parsedDate.Day())
}

func TestTimeReportEntryDalFromCSVLine(t *testing.T) {
	hours := math.Floor(gofakeit.Float64Range(1.0, 24.0)*100) / 100
	date := gofakeit.Date()
	dateString := date.Format(InputDateLayout)
	employeeID := gofakeit.Number(1, 50)
	group := []JobGroup{JobGroup(A), JobGroup(B)}[gofakeit.Number(0, 1)]

	input := []string{
		dateString,
		fmt.Sprintf("%f", hours),
		fmt.Sprintf("%d", employeeID),
		fmt.Sprintf("%s", group),
	}

	parsedEntry, err := TimeReportEntryDalFromCSVLine(input)

	assert.Equal(t, nil, err)
	assert.Equal(t, date.Year(), parsedEntry.Date.Year())
	assert.Equal(t, date.Month(), parsedEntry.Date.Month())
	assert.Equal(t, date.Day(), parsedEntry.Date.Day())
	assert.Equal(t, hours, parsedEntry.Hours)
	assert.Equal(t, employeeID, parsedEntry.EmployeeID)
	assert.Equal(t, group, parsedEntry.Group)
}
