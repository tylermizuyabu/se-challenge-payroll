package types

import (
	"fmt"
	"time"
)

const OutputDataLayout string = "2006-01-02"

type PayPeriodDto struct {
	StartDate FormattedTime `json:"startDate"`
	EndDate   FormattedTime `json:"endDate"`
}

func (pp *PayPeriodDto) Equal(pp2 *PayPeriodDto) bool {
	return pp.StartDate.Equal(pp2.StartDate) && pp.EndDate.Equal(pp2.EndDate)
}

type FormattedTime time.Time

func (t *FormattedTime) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%q", t.String())), nil
}

func (t *FormattedTime) String() string {
	return time.Time(*t).Format(OutputDataLayout)
}

func (t *FormattedTime) Equal(t2 FormattedTime) bool {
	tt := time.Time(*t)
	tt2 := time.Time(t2)
	return tt.Equal(tt2)
}
