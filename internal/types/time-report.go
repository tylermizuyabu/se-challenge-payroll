package types

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

const dateLayoutSeperator string = "/"
const InputDateLayout string = "02/01/2006"

// TimeReportDal struct is the database entity for a TimeReport
type TimeReportDal struct {
	ID            int `gorm:"primary_key"`
	ReportEntries []TimeReportEntryDal
}

// TimeReportEntryDal struct is the database entity for an entry in the time report (a single line in the time report file)
type TimeReportEntryDal struct {
	Date            time.Time `gorm:"primary_key"`
	Hours           float64
	EmployeeID      int `gorm:"primary_key"`
	Group           JobGroup
	TimeReportDalID int
}

func TimeReportEntryDalFromCSVLine(line []string) (*TimeReportEntryDal, error) {
	date, err := parseDateString(line[0])
	if err != nil {
		return nil, err
	}
	hours, err := strconv.ParseFloat(line[1], 64)
	if err != nil {
		return nil, InvalidHours
	}
	employeeID, err := strconv.Atoi(line[2])
	if err != nil {
		return nil, InvalidEmployeeId
	}

	jg := JobGroup(line[3])
	if !jg.Valid() {
		return nil, InvalidGroup
	}

	entry := &TimeReportEntryDal{
		Date:       *date,
		Hours:      hours,
		EmployeeID: employeeID,
		Group:      jg,
	}
	return entry, nil
}

func parseDateString(dateString string) (*time.Time, error) {
	dateComponents := strings.Split(dateString, dateLayoutSeperator)

	day, err := strconv.Atoi(dateComponents[0])
	if err != nil {
		return nil, err
	}
	month, err := strconv.Atoi(dateComponents[1])
	if err != nil {
		return nil, err
	}
	year, err := strconv.Atoi(dateComponents[2])
	if err != nil {
		return nil, err
	}

	date, err := time.Parse(InputDateLayout, fmt.Sprintf("%02d%s%02d%s%02d", day, dateLayoutSeperator, month, dateLayoutSeperator, year))
	if err != nil {
		return nil, InvalidDateForm
	}
	return &date, nil
}
