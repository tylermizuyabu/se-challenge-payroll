package types

type Config struct {
	PGConfig          `yaml:"pg"`
	ServerPort        int    `yaml:"port" env:"PAYROLL_REST_PORT" env-default:"8000"`
	TimeReportFileKey string `yaml:"time-report-file-key" env:"PAYROLL_TIME_REPORT_FILE_KEY" env-default:"time-report"`
	Production        bool   `yaml:"prod" env:"PAYROLL_PROD" env-default="false"`
}

type PGConfig struct {
	Host     string `yaml:"host" env:"POSTGRES_HOST" env-default:"localhost"`
	Port     string `yaml:"port" env:"POSTGRES_PORT" env-default:"5432"`
	User     string `yaml:"user" env:"POSTGRES_USER" env-default:"user"`
	Password string `yaml:"password" env:"POSTGRES_PASSWORD"`
	Database string `yaml:"database" env:"POSTGRES_DATABASE" env-default:"payroll-db"`
	SSLMode  string `yaml:"ssl-mode" env:"POSTGRES_SSL_MODE" env-default:"disable"`
}
